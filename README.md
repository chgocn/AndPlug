# AndPlug
[![Build Status](https://travis-ci.org/ourbeehive/AndPlug.png?branch=master)](https://travis-ci.org/ourbeehive/AndPlug)

A Practice of Android Modularization [中文版](https://github.com/ourbeehive/AndPlug/blob/master/README_CN.md)

## App
App is the demo Android Application for AndPlug, show how to use the module libs and how to setup an App with the modules. [more](https://github.com/ourbeehive/AndPlug/App/blob/master/README_CN.md)
[![Google Play](https://developer.android.com/images/brand/en_app_rgb_wo_60.png)](https://play.google.com/store/apps/details?id=)

## AppAA
App is the demo Android Application show how to setup an App with AndroidAnnotations(AA). [more](https://github.com/ourbeehive/AndPlug/AppAA/blob/master/README_CN.md)
 
## BaiduLoc
BaiduLoc is the lib support location provider powered by [Baidu locsdk](http://developer.baidu.com/map/index.php?title=android-locsdk). [more](https://github.com/ourbeehive/AndPlug/BaiduLoc/blob/master/README.md)

## BaiduMap
BaiduMap is the lib support map provider powered by [Baidu map sdk](http://developer.baidu.com/map/index.php?title=androidsdk). [more](https://github.com/ourbeehive/AndPlug/BaiduMap/blob/master/README.md)

## Lib
Lib is the lib include some utils and widgets. [more](https://github.com/ourbeehive/AndPlug/Lib/blob/master/README.md)

## GetuiPush
GetuiPush is the lib support message push provider powered by [Getui](http://www.getui.com/). [more](https://github.com/ourbeehive/AndPlug/Getui/blob/master/README.md)

## UmengFeedback
UmengFeedback is the lib support user feedback provider powered by [Umeng Feedback](http://www.umeng.com/component_feedback). [more](https://github.com/ourbeehive/AndPlug/UmengFeedback/blob/master/README.md)

## UmengUpdate
UmengUpdate is the lib support app auto update provider powered by [Umeng Update](http://www.umeng.com/component_update). [more](https://github.com/ourbeehive/AndPlug/UmengUpdate/blob/master/README.md)

## UniversalImage
UniversalImage is the lib support image cache network load and image post. [more](https://github.com/ourbeehive/AndPlug/UniversalImage/blob/master/README.md)

## VoiceIflytek
VoiceIflytek is the lib support tts provider powered by [Iflytek](http://www.xfyun.cn/). [more](https://github.com/ourbeehive/AndPlug/VoiceIflytek/blob/master/README.md)
